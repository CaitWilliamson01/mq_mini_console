package unigame;

class MainMenu extends Room {
  static final String path = Globals.assetspath+"Tiles/MainMenu/";

    UniGame p3;

  public MainMenu(UniGame p3){
    super(p3.loadImage(path+"MainMenu.png"), p3);
  }

}